Connect 5V supply to X1 (IN). Care for correct polarity. To protect the electronics, a diode clamps the input voltage and causes a short circuit in case of wrong polarity. Better use a fused supply.

Connect main LED Stripe to X2 (OUT: +,-,D) which shares the Arduino data pin 0 with JP1

JP2...4 can be used for additional LED Stripes on separate data pins 1...3. But make sure to keep the total power consuption in mind.

Before being able to control the LED stripes via the data pins, you must enable the level shifter by setting data pin 6 high

