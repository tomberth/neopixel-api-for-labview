/*
  UDP Test
 
 This sketch receives UDP message strings via multicast and prints them 
 to the serial port.
  
 This code is based on Michael Margolis' public domain UDP example.
 
 */


#include <SPI.h>          // needed for Arduino versions later than 0018
#include <Ethernet.h>
#include <EthernetUdp.h>  // UDP library from: bjoern@cs.stanford.edu 12/30/2008


// Enter a MAC address and IP address for your controller below.
// The IP address will be dependent on your local network:
byte mac[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };
IPAddress local_ip(10,21,31,177);
IPAddress dns(10,21,31,10);
IPAddress gateway(10,21,31,10);
IPAddress subnet(255,255,255,0);

unsigned int localPort = 58432;             // local port to listen on

// buffers for receiving and sending data
char packetBuffer[UDP_TX_PACKET_MAX_SIZE];  //buffer to hold incoming packet,

// An EthernetUDP instance to let us send and receive packets over UDP
EthernetUDP Udp;

void setup() {
  // start the Ethernet and UDP:
  Ethernet.begin(mac,local_ip,dns,gateway,subnet);
  Udp.begin(localPort);

  Serial.begin(9600);
}

void loop() {
  // if there's data available, read a packet
  int packetSize = Udp.parsePacket();
  if(packetSize)
  {
    // read the packet into packetBufffer
    Udp.read(packetBuffer,UDP_TX_PACKET_MAX_SIZE);
    Serial.print(packetBuffer);

    //Serial.print("Received packet of size ");
    //Serial.println(packetSize);
    Serial.print(" (from ");
    IPAddress remote = Udp.remoteIP();
    for (int i =0; i < 4; i++)
    {
      Serial.print(remote[i], DEC);
      if (i < 3)
      {
        Serial.print(".");
      }
    }
    Serial.print(", port ");
    Serial.print(Udp.remotePort());
    Serial.println(")");

  }
  delay(10);
}
