# NeoPixel API for LabVIEW

API and demo for accessing Adafruit's NeoPixel LEDs from NI LabVIEW(tm)

Detailed information about NeoPixel can be found here: 

- https://www.adafruit.com/category/168
  - http://learn.adafruit.com/adafruit-neopixel-uberguide/overview
  - https://github.com/adafruit/Adafruit_NeoPixel

## :pencil: Description and History

The NeoPixel API for LabVIEW provides a simple standard environment for operating NeoPixel LEDs from LabVIEW. A system will consist of

- Off-the-shelve Arduino hardware (Arduino MKR Zero with ethernet shield)
- NeoPixel LED stripe(s)
- LabVIEW software that sends commands via plain ethernet (UDP/TCP) from the LabVIEW project to the Arduino
- Arduino software (written in the Arduino IDE or if possible in LabVIEW as well) managing the LED stripe communication and implementing different light scenes (see below)

To cater to different states within a LabVIEW application, the API will provide functions to select from different "scenes" like
- Idle (very low base brightness with occasional flashes like twinkling stars)
- Static (well, as it says: static light with defined color and brightness)
- Loading (something like the Windows hourglass / mouse-arrow)
- Pulse (pulsating light in a static color)
- Progress (like a progress bar filling the illuminated object with light)

The idea for this API was born by Julian. Here's his story:

> I loved case modding since I was a child. That hasn't change until today. As a (more-or-less) grown-up engineer, I want my test bench to be an eye catcher. The first step already took place: At work, we illuminate our control racks with static blue LED stripes. 
> 
> However, when I heard about NeoPixel, I realized that this digital RGB technology has the potential to take the story to the next level. What about representing the test progress in the rack illumination? Or the startup/loading procedure? No problem with Arduino and NeoPixel. And we're sure that we`re not the only ones who want to bring some fancy light into their boring dark test benches or whatever.
> 
> ![alt text](doc/pictures/testbench.jpg "Test Bench")
> 
> ![alt text](doc/drawings/setup.png "Schematics")

The idea of making this API an open-source project and pursuing it with the [Wuerzburg LabVIEW User Group](http://bit.ly/WUELUG) came from Joerg:

> In the past few months, we have seen a very positive development in the LabVIEW ecosystem: More and more drivers, helpers and also larger libraries are published as open-source packages and made available to the community. NI supports this trend with the LabVIEW Community Edition, which can be used free of charge for non-commercial projects and which, with the updated LINX toolkit, is aimed strongly at the maker scene.
> 
> Let's start a similar topic together and learn - as a group - how to build an open-source project, pursue it, collaborate on it and ultimately benefit from it. Even in our professional lives.

## :rocket: Installation

### :building_construction: Hardware

Even though the code should be executable on various Arduino controllers (and has been tested on some!), our harware of choice is as follows:

-  Arduino MKR Zero + Arduino MKR ETH Shield

### :wrench: LabVIEW 2014

The LabVIEW VIs are maintained in LabVIEW 2014.

### :link: Dependencies

For LabVIEW: None at this point in time.

For Arduino: See the #include defitions in the .ino files:

- SPI.h
- Ethernet.h
- EthernetUdp.h
- FastLED.h



## :bulb: Usage

Run the UDP Test sketch on your Arduino and the UDP Test VI on your Pc. Make sure to update IP addresses according to your local setup.

## :construction: Maintenance 

This repository is maintained by [HAMPEL SOFTWARE ENGINEERING](https://www.hampel-soft.com) on behalf of the Wuerzburg LabVIEW User Group. 


## :busts_in_silhouette: Contributing 

We welcome every and any contribution. On our Dokuwiki, we compiled detailed information on 
[how to contribute](https://dokuwiki.hampel-soft.com/processes/collaboration). 
Please get in touch at (office@hampel-soft.com) for any questions.


##  :beers: Credits

* Julian Lange (Siemens Energy)
* Bence Bartho (Hampel Software Engineering)
* Joerg Hampel (Hampel Software Engineering)


## :page_facing_up: License 

This project is licensed under a modified BSD License - see the [LICENSE](LICENSE) file for details.
